FROM consul:1.0.7

ENV VAULT_VERSION=0.10.0
ENV CONSUL_TEMPLATE_VERSION=0.19.4

RUN curl https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.zip -o consul-template.zip && \
    unzip consul-template.zip -d /bin && \
    rm -rf consul-template.zip && \
    curl https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip -o vault.zip && \
    unzip vault.zip -d /bin && \
    rm -rf vault.zip && \
    mkdir -p /consul-template/config && \
    mkdir -p /consul-template/templates

COPY consul-template/config/config.hcl /consul-template/config/config.hcl
COPY consul-template/templates /consul-template/templates

RUN chown -R consul:consul /consul-template

COPY cloud-ca.crt /usr/local/share/ca-certificates/cloud-ca.crt
RUN apk -U add jq && update-ca-certificates

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
COPY ready-check.sh /usr/local/bin/ready-check.sh
CMD []
