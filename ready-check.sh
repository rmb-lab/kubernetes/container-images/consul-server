#!/bin/sh
set -e

CONSUL_TOKEN=`cat /consul/config/consul.json | jq -r .acl_agent_token`

CLUSTER_DATA=`curl -s -k -H "X-Consul-Token: ${CONSUL_TOKEN}" https://127.0.0.1:8500/v1/operator/autopilot/health | jq .Servers[]`
LEADER_INDEX=`echo ${CLUSTER_DATA} | jq '. | select(.Leader==true) | .LastIndex'`

if [ -z "${LEADER_INDEX}" ]; then
  echo "No leader so can't wait for matching index"
  exit 1
fi

HOSTNAME=`hostname`
MY_INDEX=`echo ${CLUSTER_DATA} | jq ". | select(.Name==\"$HOSTNAME\") | .LastIndex"`

if [ -z "${MY_INDEX}" ]; then
  echo "Could not find my index, I'm not joined in a cluster yet"
  exit 2
fi

if [ ${LEADER_INDEX} -ne ${MY_INDEX} ]; then
  echo "I do not have a matching index yet."
  exit 3
fi

