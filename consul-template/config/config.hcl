vault {
  renew_token = true
}
exec {
  kill_signal = "SIGTERM"
}
template {
  source = "/consul-template/templates/consul.json.ctmpl"
  destination = "/consul/config/consul.json"
  wait {
    min = "10s"
    max = "60s"
  }
}
template {
  source = "/consul-template/templates/consul.crt.ctmpl"
  destination = "/consul/config/consul.crt"
}
template {
  source = "/consul-template/templates/consul.key.ctmpl"
  destination = "/consul/config/consul.key"
}
