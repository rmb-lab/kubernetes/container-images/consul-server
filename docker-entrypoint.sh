#!/usr/bin/dumb-init /bin/sh
set -e

# Note above that we run dumb-init as PID 1 in order to reap zombie processes
# as well as forward signals to all processes in its session. Normally, sh
# wouldn't do either of these functions so we'd leak zombies as well as do
# unclean termination of all our sub-processes.
# As of docker 1.13, using docker run --init achieves the same outcome.

# CONSUL_BIND_ADDRESS=$(ip -o -4 addr list $CONSUL_BIND_INTERFACE | head -n1 | awk '{print $4}' | cut -d/ -f1)
# CONSUL_BIND="-bind=$CONSUL_BIND_ADDRESS"
# echo "==> Found address '$CONSUL_BIND_ADDRESS' for interface '$CONSUL_BIND_INTERFACE', setting bind option..."

# CONSUL_CLIENT_ADDRESS=$(ip -o -4 addr list $CONSUL_CLIENT_INTERFACE | head -n1 | awk '{print $4}' | cut -d/ -f1)
# CONSUL_CLIENT="-client=$CONSUL_CLIENT_ADDRESS"
# echo "==> Found address '$CONSUL_CLIENT_ADDRESS' for interface '$CONSUL_CLIENT_INTERFACE', setting client option..."

# CONSUL_DATA_DIR is exposed as a volume for possible persistent storage. The
# CONSUL_CONFIG_DIR isn't exposed as a volume but you can compose additional
# config files in there if you use this image as a base, or use CONSUL_LOCAL_CONFIG
# below.
CONSUL_DATA_DIR=/consul/data
CONSUL_CONFIG_DIR=/consul/config

# If the data or config dirs are bind mounted then chown them.
# Note: This checks for root ownership as that's the most common case.
if [ "$(stat -c %u /consul/data)" != "$(id -u consul)" ]; then
        chown consul:consul /consul/data
fi
if [ "$(stat -c %u /consul/config)" != "$(id -u consul)" ]; then
    chown consul:consul /consul/config
fi

if [ -z "$VAULT_TOKEN" ]; then
  export VAULT_TOKEN=$(vault write -field token auth/kubernetes/login role=consul-server jwt=`cat /var/run/secrets/kubernetes.io/serviceaccount/token`)
fi

su-exec consul:consul consul-template -config "/consul-template/config" -exec "consul agent -data-dir=\"$CONSUL_DATA_DIR\" -config-dir=\"$CONSUL_CONFIG_DIR\" $CONSUL_BIND $CONSUL_CLIENT \"$@\""
